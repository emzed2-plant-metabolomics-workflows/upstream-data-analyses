#### Normalise expression data ####

# Import libraries
library(edgeR)
library(limma)

# Import data
countTable <- read.table("Count_QC-raw-count.txt",sep="\t",header=T,row.names=1)
countTable <- round(countTable)

meta <- read.table("metadata.txt",header=T)
meta <- meta[rep(1:nrow(meta),each=5),]
meta$Repeat <- 1:5
meta[,1] <- paste(meta[,1],"R",1:5,sep="")
rownames(meta) <- paste("X",meta[,1],sep="")
meta <- meta[colnames(countTable),]

# Limma analysis
condition <- relevel(meta$Condition,ref="Axenic")
design <- model.matrix(~condition)
colnames(design) <- sub("condition","",colnames(design))

dge <- DGEList(counts=countTable)
good <- filterByExpr(dge,design)
dge <- dge[good,,keep.lib.sizes=FALSE]
dge <- calcNormFactors(dge)

v <- voom(dge,design,plot=FALSE)

fit <- lmFit(v, design)
fit <- eBayes(fit)

exprCount <- v$E
colnames(exprCount) <- paste(meta[colnames(v$E),2],"R",meta[colnames(v$E),3],sep="")
colnames(exprCount)[grepl("Axenic",colnames(exprCount))] <- paste("AxenicR",1:10,sep="")

#### Correlations ####

# Expr-expr spearman
library(parallel)
cl <- makeCluster(64)

clusterExport(cl,"exprCount")

scorMatrix <- parApply(cl,exprCount,1,function(x) apply(exprCount,1,function(y) cor.test(x,y,method="spearman")$estimate))
scorMatrix <- as.data.frame(scorMatrix)
scorPvals <- parApply(cl,exprCount,1,function(x) apply(exprCount,1,function(y) cor.test(x,y,method="spearman")$p.value))
scorPvals <- as.data.frame(scorPvals)

# Expr-meta spearman
metaCount <- read.table("exB7_crid_log2_norm_area_data_matrix.csv",sep=";",header=T,row.names=1)
colnames(metaCount) <- sub("leaf","Leaf",colnames(metaCount))
colnames(metaCount) <- sub("Leafaxenic","Axenic",colnames(metaCount))
colnames(metaCount) <- sub("Leafcommunity","Community",colnames(metaCount))
colnames(metaCount) <- sub("LeafFr1","Fr1",colnames(metaCount))
colnames(metaCount)[grepl("Leafax_control",colnames(metaCount))] <- paste("AxenicR",6:10,sep="")
metaCount <- metaCount[,colnames(exprCount)]
clusterExport(cl,"metaCount")

scorMeta <- parApply(cl,exprCount,1,function(x) apply(metaCount,1,function(y) cor.test(x,y,method="spearman")$estimate))
scorMeta <- as.data.frame(scorMeta)
scorMetaPvals <- parApply(cl,exprCount,1,function(x) apply(metaCount,1,function(y) cor.test(x,y,method="spearman")$p.value))
scorMetaPvals <- as.data.frame(scorMetaPvals)

stopCluster(cl)

#### Filtering + Network ####

# Import gene sets
wrky <- read.table("WRKY_TFs.txt",sep="\t",header=T,stringsAsFactors=F)[,1]
wrky <- wrky[wrky%in%colnames(scorMatrix)]
wrky <- c(wrky,"AT1G18570","AT1G74080","AT3G23250")
gnsr <- read.table("GNSR.txt",stringsAsFactors=F)[,1]

# WRKY - gene - GNSR - Metabolite network
n = 2
cutoff = 0.85

wrky_gene <- scorMatrix[!rownames(scorMatrix)%in%wrky,wrky]
wrky_gene_net <- cbind(expand.grid(rownames(wrky_gene),colnames(wrky_gene),stringsAsFactors=F),cor=unlist(wrky_gene),stringsAsFactors=F)
wrky_gene_net <- wrky_gene_net[order(abs(wrky_gene_net[,3]),decreasing=T),]
wrky_gene_net_filt <- do.call(rbind,lapply(wrky,function(x) wrky_gene_net[wrky_gene_net[,2]==x,][1:n,]))
wrky_gene_net_filt <- wrky_gene_net_filt[wrky_gene_net_filt[,3]>=cutoff,]
wrky_targets <- unique(wrky_gene_net_filt[,1])

gnsr_meta <- scorMeta[,gnsr]
write.table(gnsr_meta,"gnsr_meta.txt")
gnsr_meta_p <- scorMetaPvals[,gnsr]
gnsr_meta_padj <- matrix(p.adjust(unlist(gnsr_meta_p),method='fdr'),nrow=nrow(gnsr_meta_p))
write.table(gnsr_meta_padj,"gnsr_meta_padj.txt")
targ_meta <- scorMeta[,wrky_targets]
gnsr_wrky <- scorMatrix[wrky,gnsr]

gnsr_meta_net <- cbind(expand.grid(rownames(gnsr_meta),colnames(gnsr_meta),stringsAsFactors=F),cor=unlist(gnsr_meta),stringsAsFactors=F)
targ_meta_net <- cbind(expand.grid(rownames(targ_meta),colnames(targ_meta),stringsAsFactors=F),cor=unlist(targ_meta),stringsAsFactors=F)
gnsr_wrky_net <- cbind(expand.grid(rownames(gnsr_wrky),colnames(gnsr_wrky),stringsAsFactors=F),cor=unlist(gnsr_wrky),stringsAsFactors=F)

gnsr_meta_net <- gnsr_meta_net[order(abs(gnsr_meta_net[,3]),decreasing=T),]
targ_meta_net <- targ_meta_net[order(abs(targ_meta_net[,3]),decreasing=T),]
gnsr_wrky_net <- gnsr_wrky_net[order(abs(gnsr_wrky_net[,3]),decreasing=T),]

gnsr_meta_net_filt <- do.call(rbind,lapply(gnsr,function(x) gnsr_meta_net[gnsr_meta_net[,2]==x,][1:n,]))
gnsr_meta_net_filt <- gnsr_meta_net_filt[gnsr_meta_net_filt[,3]>=cutoff,]
targ_meta_net_filt <- do.call(rbind,lapply(wrky_targets,function(x) targ_meta_net[targ_meta_net[,2]==x,][1:n,]))
targ_meta_net_filt <- targ_meta_net_filt[targ_meta_net_filt[,3]>=cutoff,]
gnsr_wrky_net_filt <- do.call(rbind,lapply(gnsr,function(x) gnsr_wrky_net[gnsr_wrky_net[,2]==x,][1:n,]))
gnsr_wrky_net_filt <- gnsr_wrky_net_filt[gnsr_wrky_net_filt[,3]>=cutoff,]

meta <- rownames(scorMeta)
meta_gene <- scorMeta[meta,]
meta_gene_net <- cbind(expand.grid(rownames(meta_gene),colnames(meta_gene),stringsAsFactors=F),cor=unlist(meta_gene),stringsAsFactors=F)
meta_gene_net_filt <- meta_gene_net[meta_gene_net[,3]>=cutoff,]

all_net <- rbind(wrky_gene_net_filt,gnsr_meta_net_filt,targ_meta_net_filt,gnsr_wrky_net_filt,meta_gene_net_filt,stringsAsFactors=F)
all_net <- all_net[all_net[,1]!=all_net[,2],]

library(igraph)
network <- graph_from_edgelist(as.matrix(all_net[,1:2]),directed=F)
vertex_attr(network,"type") <- c("gene","gnsr","wrky","both","meta")[V(network)$name%in%gnsr +(2*V(network)$name%in%wrky) +(4*V(network)$name%in%rownames(scorMeta))+1]
edge_attr(network,"cor") <- round(all_net[,3],3)
write.graph(network,"gnsr-meta-wrky.graphml","graphml")

# GNSR correlations w/ genes
for(gene in gnsr){
    cors <- sort(scorMatrix[gene,],decreasing=T)[2:11]
    bests <- names(cors)
    pdf(paste("gnsr_",gene,".pdf",sep=""),width=7,height=7)
    for(best in bests){
        plot(unlist(exprCount[gene,]),unlist(exprCount[best,]),xlab=gene,ylab=best,main=round(cors[best],4))
    }
    dev.off()
}

# WRKY correlations w/ genes
for(gene in wrky){
    cors <- sort(scorMatrix[gene,],decreasing=T)[2:11]
    bests <- names(cors)
    pdf(paste("wrky_",gene,".pdf",sep=""),width=7,height=7)
    for(best in bests){
        plot(unlist(exprCount[gene,]),unlist(exprCount[best,]),xlab=gene,ylab=best,main=round(cors[best],4))
    }
    dev.off()
}

# Gene/Meta correlations
bests <- which((scorMeta>0.85),arr.ind=T)
pdf("meta_gene.pdf",width=7,height=7)
for(i in 1:nrow(bests)){
    meta = rownames(scorMeta)[bests[i,1]]
    gene = colnames(scorMeta)[bests[i,2]]
    plot(unlist(metaCount[meta,]),unlist(exprCount[gene,]),xlab=meta,ylab=gene,main=round(scorMeta[meta,gene],4))
}
dev.off()

# Correlations for nodes in the graph
pdf("network_correlations.pdf",width=7,height=7)
for(i in 1:nrow(wrky_gene_net_filt)){
    plot(unlist(exprCount[wrky_gene_net_filt[i,1],]),unlist(exprCount[wrky_gene_net_filt[i,2],]),xlab=wrky_gene_net_filt[i,1],ylab=wrky_gene_net_filt[i,2],main=paste(round(scorMatrix[wrky_gene_net_filt[i,1],wrky_gene_net_filt[i,2]],4),round(scorPvals[wrky_gene_net_filt[i,1],wrky_gene_net_filt[i,2]],4)))
}
for(i in 1:nrow(gnsr_meta_net_filt)){
    plot(unlist(metaCount[gnsr_meta_net_filt[i,1],]),unlist(exprCount[gnsr_meta_net_filt[i,2],]),xlab=gnsr_meta_net_filt[i,1],ylab=gnsr_meta_net_filt[i,2],main=paste(round(scorMeta[gnsr_meta_net_filt[i,1],gnsr_meta_net_filt[i,2]],4),round(scorMetaPvals[gnsr_meta_net_filt[i,1],gnsr_meta_net_filt[i,2]],4)))
}
for(i in 1:nrow(targ_meta_net_filt)){
    plot(unlist(metaCount[targ_meta_net_filt[i,1],]),unlist(exprCount[targ_meta_net_filt[i,2],]),xlab=targ_meta_net_filt[i,1],ylab=targ_meta_net_filt[i,2],main=paste(round(scorMeta[targ_meta_net_filt[i,1],targ_meta_net_filt[i,2]],4),round(scorMetaPvals[targ_meta_net_filt[i,1],targ_meta_net_filt[i,2]],4)))
}
for(i in 1:nrow(gnsr_wrky_net_filt)){
    plot(unlist(exprCount[gnsr_wrky_net_filt[i,1],]),unlist(exprCount[gnsr_wrky_net_filt[i,2],]),xlab=gnsr_wrky_net_filt[i,1],ylab=gnsr_wrky_net_filt[i,2],main=paste(round(scorMatrix[gnsr_wrky_net_filt[i,1],gnsr_wrky_net_filt[i,2]],4),round(scorPvals[gnsr_wrky_net_filt[i,1],gnsr_wrky_net_filt[i,2]],4)))
}
for(i in 1:nrow(meta_gene_net_filt)){
    plot(unlist(metaCount[meta_gene_net_filt[i,1],]),unlist(exprCount[meta_gene_net_filt[i,2],]),xlab=meta_gene_net_filt[i,1],ylab=meta_gene_net_filt[i,2],main=paste(round(scorMeta[meta_gene_net_filt[i,1],meta_gene_net_filt[i,2]],4),round(scorMetaPvals[meta_gene_net_filt[i,1],meta_gene_net_filt[i,2]],4)))
}
dev.off()

#### Ordination plots ####
library(vegan)
library(phylloR)

# Filter counts to stabilise the ordination
metaCount2 <- metaCount[,which(!grepl("Community",colnames(metaCount)))]
metaCount2 <- metaCount2[names(sort(apply(metaCount,1,var),decreasing=T)[1:1000]),]

exprCount2 <- exprCount[,which(!grepl("Community",colnames(exprCount)))]
exprCount2 <- exprCount2[names(sort(apply(exprCount,1,var),decreasing=T)[1:2000]),]

# Data grouping
group <- sub("R.+","",colnames(exprCount2))
nos <- as.numeric(as.factor(group))

# PCAs
metaPCA <- prcomp(t(metaCount2))
exprPCA <- prcomp(t(exprCount2))

# PCoAs
metaDM <- vegdist(t(metaCount2),method="euclidean")
exprDM <- vegdist(t(exprCount2),method="euclidean")

# MDSs
metaMDS <- monoMDS(metaDM)
exprMDS <- monoMDS(exprDM)

# Graphics set up
colors <- leafTaxonomy[group,]$Color
colors[is.na(colors)] <- "#000000"
colors[group=="Fr1"] <- "#00BA62"

# Pairwise by condition
pdf("pairwise_ordination_plots.pdf",width=8,height=4)
par(mfrow=c(1,2))
stats <- c()
control <- which(nos==1)[1:5]
for(i in 2:max(nos)){
    metaSub <- metaCount2[,c(control,which(nos==i))]
    subPCA <- prcomp(t(metaSub))
    adn <- adonis(t(metaSub)~group[c(control,which(nos==i))],method="euclidean")
    stats <- c(stats,adn$aov.tab$R2[1],adn$aov.tab$Pr[1])
    plot(subPCA$x[,1:2],pch=20,col=colors[c(control,which(nos==i))],main=paste(levels(as.factor(group))[i],"Metabolites"),sub=paste(formatC(100*adn$aov.tab$R2[1],digits=3),"%; P-value: ",formatC(adn$aov.tab$Pr[1],3),sep=""))

    exprSub <- exprCount2[,c(control,which(nos==i))]
    subPCA <- prcomp(t(metaSub))
    adn <- adonis(t(exprSub)~group[c(control,which(nos==i))],method="euclidean")
    stats <- c(stats,adn$aov.tab$R2[1],adn$aov.tab$Pr[1])
    plot(subPCA$x[,1:2],pch=20,col=colors[c(control,which(nos==i))],main=paste(levels(as.factor(group))[i],"Expression"),sub=paste(formatC(100*adn$aov.tab$R2[1],digits=3),"%; P-value: ",formatC(adn$aov.tab$Pr[1],3),sep=""))
}
dev.off()

# Stats
stats <- matrix(stats,nrow=4)
colnames(stats) <- levels(as.factor(group))[2:max(nos)]
rownames(stats) <- c("Meta Effect Size","Meta p-value","Expr Effect Size","Expr p-value")
write.table(stats,"pca-stats.txt")

# More graphics setup
metaAlpha <- colors
metaAlpha[metaAlpha!="#000000"] <- paste(colors[colors!="#000000"],as.hexmode(round(stats[1,group[group!="Axenic"]]/max(stats[1,])*255)),sep="")
exprAlpha <- colors
exprAlpha[exprAlpha!="#000000"] <- paste(colors[colors!="#000000"],as.hexmode(round(stats[3,group[group!="Axenic"]]/max(stats[3,])*255)),sep="")

# Plot all points
pdf("ordination_plots.pdf",width=10,height=10)
plot(metaPCA$x[,1:2],col=metaAlpha,pch=20,main="Metabolite PCA")
plot(metaPCA$x[,1:2],type="n",main="Metabolite PCA")
text(metaPCA$x[,1:2],colnames(metaCount2),col=colors)
plot(exprPCA$x[,1:2],col=metaAlpha,pch=20,main="Expression PCA")
plot(exprPCA$x[,1:2],type="n",main="Expression PCA")
text(exprPCA$x[,1:2],colnames(exprCount2),col=colors)
plot(metaMDS$points,col=exprAlpha,pch=20,main="Metabolite MDS")
plot(metaMDS$points,type="n",main="Metabolite MDS")
text(metaMDS$points,colnames(metaCount2),col=colors)
plot(exprMDS$points,col=exprAlpha,pch=20,main="Expression MDS")
plot(exprMDS$points,type="n",main="Expression MDS")
text(exprMDS$points,colnames(exprCount2),col=colors)
dev.off()

