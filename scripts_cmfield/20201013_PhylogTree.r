cairo_pdf("fancy_tree.pdf",width=10,height=10)

# Import the data
data <- read.table("Data_for_fancy_tree.csv",sep=";",comment.char="",header=T,row.names=1)

library(phylloR)
# Fix the tree labels
leafPhylogeny$tip.label[leafPhylogeny$tip.label=="Leaf1773"] = "Leaf177"

# Make the relevant subtree
rownames(data) <- sub("L","Leaf",rownames(data))
rownames(data)[rownames(data)=="Fr1"] <- "FR1"
tree <- keep.tip(leafPhylogeny,rownames(data))

# Rearrange data
data$Color <- leafTaxonomy[rownames(data),]$Color
data$Color[length(data$Color)] <- "#00BA62"
data <- data[tree$tip.label,]

# Work out scale
depth = max(node.depth.edgelength(tree))

# Rename the tips
tree$tip.label <- leafTaxonomy[tree$tip.label,]$Name

# Plot the tree
plot(tree,type="fan",x.lim=c(-3*depth,3*depth),y.lim=c(-3*depth,3*depth),label.offset=0.1*depth,cex=0.5,align.tip.label=T)
radAngle = 2*pi/nrow(data)
degAngle = 360/nrow(data)
radAngles = radAngle*(0:nrow(data))
degAngles = degAngle*(0:nrow(data))

# Work out scale
codepth = (depth*1.1) +  max(strwidth(tree$tip.label,cex=0.5))
increment = 0.02

xco = codepth*1.1*cos(radAngles)
yco = codepth*1.1*sin(radAngles)
points(xco,yco,pch=19,col=data$Color)

# Function to add coloured circle data
# data              A vector of data to be plotted
# z.min             The minimum value of the color scale
# z.max             The maximum value of the color scale
# rou1              The distance from the centre to the inside of the circle
# rou2              The distance from the centre to the outside of the circle
# z.colors          A 32-element vector of colors
# scalebar          A logical indicatting whether to include a scalebar or not.
# scalebar.pos      The c(x,y) coordinates for the bottom left of the scalebar
# scalebar.size     The c(x,y) width and height of the scalebar
# scalebar.label    The name of the scalebar
draw.color.circle <- function(data,z.min=NULL,z.max=NULL,rou1,rou2,z.colors,scalebar=FALSE,scalebar.pos=NULL,scalebar.size=NULL,scalebar.label=NULL){
    angle = 360/length(data)
    angles = angle*(0:length(data))

    if(is.null(z.min)){
        z.min = min(data)
    }
    if(is.null(z.max)){
        z.max = max(data)
    }
    if(scalebar){
        if(is.null(scalebar.pos) | is.null(scalebar.size)){
            cat("Scale bar requires a position and size.")
        return()
        }
    }

    # Bin data
    breaks <- z.min + ((z.max-z.min)*(0:32/32))
    binned <- cut(data,breaks,labels=F,include.lowest=T)

    # Plot data
    for(i in 1:length(data)){
        draw.sector(angles[i] - (angle/2),angles[i]+(angle/2),rou1=rou1,rou2=rou2,center=c(0,0),clock.wise=F,col=z.colors[binned[i]],border="white")
    }

    # Plot scale
    if(scalebar){
        segment = scalebar.size[1]/32
        rect(scalebar.pos[1]+(0:31*segment),scalebar.pos[2],scalebar.pos[1]+(1:32*segment),scalebar.pos[2]+scalebar.size[2],col=z.colors[1:32],border=NA)
        rect(scalebar.pos[1],scalebar.pos[2],scalebar.pos[1]+scalebar.size[1],scalebar.pos[2]+scalebar.size[2],border="black")
        text(scalebar.pos[1]+(0:4*8*segment),scalebar.pos[2]+1.5*scalebar.size[2],round(breaks[1+0:4*8],1),cex=0.5,adj=0.5)
        if(!is.null(scalebar.label)){
            text(scalebar.pos[1]+16*segment,scalebar.pos[2]-0.5*scalebar.size[2],scalebar.label,cex=0.5,adj=0.5)
        }
    }
}


# Add the CFU count
library(circlize)

draw.color.circle(data$cfu,z.min=7,z.max=11,rou1=1.2*codepth,rou2=1.3*codepth,z.colors=colorRampPalette(c("yellow","yellow4"))(32),scalebar=T,scalebar.pos=c(-3*depth,2.5*depth),scalebar.size=c(0.64,0.1*codepth),scalebar.label="log10(CFU)")

# Add the DEG count
draw.color.circle(log10(1+data$X.DEGs),z.min=0,z.max=4,rou1=1.3*codepth,rou2=1.4*codepth,z.colors=colorRampPalette(c("orange","orange4"))(32),scalebar=T,scalebar.pos=c(-3*depth,2*depth),scalebar.size=c(0.64,0.1*codepth),scalebar.label="log10(DEG)")

# Add the DAM count
draw.color.circle(log10(1+data$X.DAM),z.min=0,z.max=4,rou1=1.4*codepth,rou2=1.5*codepth,z.colors=colorRampPalette(c("red","red4"))(32),scalebar=T,scalebar.pos=c(-3*depth,1.5*depth),scalebar.size=c(0.64,0.1*codepth),scalebar.label="log10(DAM)")


#cfu <- 1+round(31*(data$cfu-min(data$cfu))/(max(data$cfu)-min(data$cfu)))
#for(i in 1:nrow(data)){
#    draw.sector(degAngles[i]-(degAngle/2),degAngles[i]+(degAngle/2),rou1=1.2*codepth,rou2=1.3*codepth,center=c(0,0),clock.wise=F,col=colorRampPalette(c("yellow","yellow4"))(32)[cfu[i]],border="white")
#}
#rect((-3*depth)+(0:31*increment),2.5*depth,(-3*depth)+(1:32*increment),2.6*depth,col=colorRampPalette(c("yellow","yellow4"))(32)[1:32],border=NA)
#rect((-3*depth),2.5*depth,(-3*depth)+(32*increment),2.6*depth,border="black")
#text((-3*depth)+(c(0,8,16,24,32)*increment),2.65*depth,round(min(data$cfu)+c(0,8,16,24,32)*(max(data$cfu)-min(data$cfu))/31,1),cex=0.5,adj=0.5)
#text((-3*depth)+(16*increment),2.45*depth,"log10(CFU)",cex=0.5,adj=0.5)

# Add the DEG count
#deg <- 1+round(31*(data$X.DEGs-min(data$X.DEGs))/(max(data$X.DEGs)-min(data$X.DEGs)))
#for(i in 1:nrow(data)){
#    draw.sector(degAngles[i]-(degAngle/2),degAngles[i]+(degAngle/2),rou1=1.3*codepth,rou2=1.4*codepth,center=c(0,0),clock.wise=F,col=colorRampPalette(c("orange","orange4"))(32)[deg[i]],border="white")
#}
#rect((-3*depth)+(0:31*increment),2.2*depth,(-3*depth)+(1:32*increment),2.3*depth,col=colorRampPalette(c("orange","orange4"))(32)[1:32],border=NA)
#rect((-3*depth),2.2*depth,(-3*depth)+(32*increment),2.3*depth,border="black")
#text((-3*depth)+(c(0,8,16,24,32)*increment),2.35*depth,round(min(data$cfu)+c(0,8,16,24,32)*(max(data$cfu)-min(data$cfu))/31,1),cex=0.5,adj=0.5)
#text((-3*depth)+(16*increment),2.15*depth,"DEGs",cex=0.5,adj=0.5)

# Add the DAM count
#dam <- 1+round(31*(data$X.DAM-min(data$X.DAM))/(max(data$X.DAM)-min(data$X.DAM)))
#for(i in 1:nrow(data)){
#        draw.sector(degAngles[i]-(degAngle/2),degAngles[i]+(degAngle/2),rou1=1.4*codepth,rou2=1.5*codepth,center=c(0,0),clock.wise=F,col=colorRampPalette(c("red","red4"))(32)[deg[i]],border="white")
#}
#rect((-3*depth)+(0:31*increment),1.9*depth,(-3*depth)+(1:32*increment),2.0*depth,col=colorRampPalette(c("red","red4"))(32)[1:32],border=NA)
#rect((-3*depth),1.9*depth,(-3*depth)+(32*increment),2.0*depth,border="black")
#text((-3*depth)+(c(0,8,16,24,32)*increment),2.05*depth,round(min(data$cfu)+c(0,8,16,24,32)*(max(data$cfu)-min(data$cfu))/31,1),cex=0.5,adj=0.5)
#text((-3*depth)+(16*increment),1.85*depth,"DEGs",cex=0.5,adj=0.5)


# Add the DEG count
#increment = 0.1
#genes <- log10(1+data$X.DEGs)
#genes <- (4*increment)*genes/4
#draw.circle(0,0,(1.4*codepth)+(1:4*increment),nv=360,border="sandybrown")
#for(i in 1:nrow(data)){
#            draw.sector(degAngles[i]-(degAngle/2),degAngles[i]+(degAngle/2),rou1=1.4*codepth,rou2=(1.4*codepth)+genes[i],center=c(0,0),clock.wise=F,col="orange",border="black")
#}
#text(((1.4*codepth)+(1:4*increment))*cos(radAngles[1]),((1.4*codepth)+(1:4*increment))*sin(radAngles[1]),c(10,100,1000,10000),adj=0.5,cex=0.5,col="sandybrown",srt=-90)

# Add the DAM count
#increment = 0.1
#metas <- log10(1+data$X.DAM)
#metas[is.na(metas)] <- 0
#metas <- (3*increment)*metas/3
#draw.circle(0,0,(1.8*codepth)+(1:3*increment),nv=360,border="tomato")
#for(i in 1:nrow(data)){
#                draw.sector(degAngles[i]-(degAngle/2),degAngles[i]+(degAngle/2),rou1=1.8*codepth,rou2=(1.8*codepth)+metas[i],center=c(0,0),clock.wise=F,col="red",border="black")
#}
#text(((1.8*codepth)+(1:3*increment))*cos(radAngles[1]),((1.8*codepth)+(1:3*increment))*sin(radAngles[1]),c(10,100,1000),adj=0.5,cex=0.5,col="tomato",srt=-90)

dev.off()
