scripts_cmfield contains two R scripts containing all code and data for:
- correlation analyses (gene-gene, metabolite-genes, transcription factors - gene)
- Ordination plots (PCA analysis)
- The basis for the phylogenetic tree of Fig. 1

#################
20201013_PhylogTree.R produces the phylogenetic tree that is the basis for main Fig.1 


#################
20201013_Final_code.R produces the correlation analyses (Fig.3a) and ordination plots (Extended Data Fig 2cd)

