scripts_bamaier contains two R scripts containing all code to make the basis for all major figures used in the publication (except Fig.1 ), as well as the data files required to run the code.

#################
20200916_GNSR_scripts_BAM.R produces graphs and results relating to the major evaluations performed on the transcriptomics data sets as well as the final pathogen susceptiblity assay. 
It contains 5 scripts that, with the exception of script 5 are dependent on the results from script 1. 

Script 1: Reads in the results from the differential gene expression analysis, filters the data for significance and produces a heatmap with all significant genes (Extended Data Figure 2a)

Script 2: Produces the same heatmap but sorted by abundance (Figure 2a)

Script 3: Produces the GNSR heatmap (Figure 2b)

Script 4: Performs the linear regression between response strength and individual gene fold-changes and plots their adjusted R�s (Figure 2d)

Script 5: Performs the logistic regression on the cfu/g_fw of Pseudomonas syringae on the different GNSR gene mutants. Only the significance values were used for Figure 4a.


#################
20200924_Metabolomics_scripts_BAM.R produces graphs and results relating to the major evaluations performed on the metabolomics data.
It contains 6 scripts that, with the exception of scripts 4-5 depend on the results from script 1

Script 1: Reads in the filtered results from the differential abundance analysis and produces a heatmap with all significant metabolites (Extended Data Figure 2b)

Script 2: Produces the GNSR associated metabolites heatmap (Figure 3b)

Script 3: Creates a heatmap of the most abundantly differentially regulated metabolite features (Extended Data Figure 6)

Script 4: Plots the results from the GNSR gene knockout plant metabolomics experiment (Figure 3c)

Script 5: Plots the results from the 2nd GNSR gene knockout plant metabolomics experiment (Extended Data Figure 8)

Script 6: Produces the same heatmap as Script 1, but sorted by abundance (Extended Data Figure 4)
